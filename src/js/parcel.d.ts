declare module "url:*" {
    const value: string;
    export default value;
}

interface PartOption {
    text: string,
    to: string,
}

interface PartActor {
    id: string,
    emotion: string,
    flip?: boolean,
}

interface Part {
    text: string,
    next?: string,
    custom_options?: string,
    options?: PartOption[],
    scene?: string,
    actor?: PartActor,
    points?: { [index: string]: number }
}

interface DialogRoles {
    [index: string]: {name: string, color: string},
}

interface DialogueTree {
    parts: { [index: string]: Part },
    characters: DialogRoles,
}

declare module "*/dialogue.yml" {
    const value: DialogueTree;
    export default value;
}

interface LevelData {
    background: string,
}

interface LevelsData {
    [index: string]: LevelData,
}

declare module "*/levels.yml" {
    const value: LevelsData;
    export default value;
}

interface CharacterData {
    name: string,
    emotions: { [index: string]: string },
}

interface CharactersData {
    [index: string]: CharacterData,
}

declare module "*/characters.yml" {
    const value: CharactersData;
    export default value;
}
import level_data from "../assets/levels/levels.yml";
import * as assets from './assets';
import * as pixi from 'pixi.js'
import { GameState } from "./gamestate";

class Level {
    texture: pixi.Texture;

    constructor(source: LevelData) {
        this.texture = pixi.Texture.from(assets.levels[source.background]);
    }
}

export default class Levels {
    static levels: { [index: string]: Level } = {};

    static load_levels(){
        for(const level_id in level_data){
            Levels.levels[level_id] = new Level(level_data[level_id]);
        }
    }

    static set_scene(scene_id: string, state: GameState){
        state.background.texture = Levels.levels[scene_id].texture;
    }
}


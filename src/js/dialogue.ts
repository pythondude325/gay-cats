import dialogue_tree from "../assets/dialogue.yml";
import * as MarkdownIt from "markdown-it";
import { GameState } from "./gamestate";
import * as handlebars from "handlebars";
import Levels from "./levels";
import Characters from "./characters";

const maintext = document.getElementById("maintext");
const options_list = document.getElementById("interactions");


class TextProcessor {
    md: MarkdownIt;
    hbs: typeof handlebars;

    processText(template: string, state: GameState): string {
        const compiled = this.hbs.compile(template);
        const context = {
            state,
            name: state.character_name,
            lover: state.lover,
            "mc": {name: state.character_name, color: "#0044e9"}
        };
        Object.assign(context, dialogue_tree.characters);
        return compiled(context);
    }

    render(template: string, state: GameState): string {
        return this.md.render(this.processText(template, state));
    }

    renderInline(template: string, state: GameState): string {
        return this.md.renderInline(this.processText(template, state));
    }

    constructor() {
        this.md = new MarkdownIt({typographer: true, html: true});
        this.hbs = handlebars.create();

        const old_this = this;
        function quote_helper(character, options){
            return new handlebars.SafeString(
                `<span class=quote style="color:${character.color}">` +
                old_this.renderInline(`${character.name}: "${options.fn(this)}"`, this.state) +
                `</span>`
            );
        }

        this.hbs.registerHelper("quote", quote_helper);
        this.hbs.registerHelper("q", quote_helper);
    }
}

const processor = new TextProcessor();

export function goto_part(part_id: string, state: GameState){
    console.log(`Going to "${part_id}"`)

    const part = dialogue_tree.parts[part_id];

    const rendered_maintext = processor.render(part.text, state);
    maintext.innerHTML = rendered_maintext;

    if(part.points !== undefined){
        for(const char in part.points){
            state.points[char] += part.points[char]
        }
    }

    if(part.scene !== undefined){
        Levels.set_scene(part.scene, state);
    }

    if(part.actor !== undefined){
        Characters.set_main_actor(part.actor, state);
    } else {
        Characters.hide_main_actor(state);
    }

    options_list.innerHTML = ""
    if(part.next !== undefined){
        const option_element = document.createElement("li");
        option_element.classList.add("interaction");
        option_element.classList.add("center");
        option_element.innerHTML = processor.renderInline("...", state);
        option_element.addEventListener("click", () => {
            goto_part(part.next, state);
        })
        options_list.appendChild(option_element);
    } else if(part.custom_options !== undefined){
        custom_interactions[part.custom_options](state);
    } else if(part.options !== undefined){
        for(const option of part.options){
            const option_element = document.createElement("li");
            option_element.classList.add("interaction");
            option_element.innerHTML = processor.renderInline(option.text, state);
            option_element.addEventListener("click", () => {
                goto_part(option.to, state);
            })
            options_list.appendChild(option_element);
        }
    }
}

const custom_interactions  = {
    name_selection(state: GameState) {
        const names = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        for(const name of names){
            const option_element = document.createElement("li");
            option_element.classList.add("interaction");
            option_element.classList.add("center");
            option_element.innerText = name;
            option_element.addEventListener("click", () => {
                state.character_name = name;
                goto_part("welcome name", state);
            });
            options_list.appendChild(option_element);
        }
    },
    choose_love(state: GameState){
        const love_options = []
        for(const id in state.points){
            if(state.points[id] > 0){
                love_options.push(id)
            }
        }
        if(love_options.length === 0){
            const option_element = document.createElement("li");
            option_element.classList.add("interaction");
            option_element.innerText = "Go back to the human world";
            option_element.addEventListener("click", () => {
                goto_part("love.kile", state);
            });
            options_list.appendChild(option_element);
        } else {
            for(const name of love_options){
                const option_element = document.createElement("li");
                option_element.classList.add("interaction");
                option_element.innerText = name;
                option_element.addEventListener("click", () => {
                    state.lover = name
                    goto_part("love." + name, state);
                });
                options_list.appendChild(option_element);
            }
        }
    },
};

export function check_dialogue(){
    function check_part(parent: string, id: string){
        if(!(id in dialogue_tree.parts)){
            console.warn(`dialogue warning: ${parent} optoin ${id} is broken`);
        }
    }

    function check_actor(parent: string, actor: PartActor){
        if(!(actor.id in Characters.characters)){
            console.warn(`${parent} invalid actor id`);
        } else {
            const actor_emotions = Characters.characters[actor.id].emotions;
            if(!(actor.emotion in actor_emotions)){
                console.warn(`${parent}: invalid actor emotion`);
            }
        }
    }

    for(const part_id in dialogue_tree.parts){
        const part = dialogue_tree.parts[part_id];

        if(part.next !== undefined){
            check_part(part_id, part.next)
        } else if(part.options !== undefined){
            for(const option of part.options){
                const option_dest = option.to
                check_part(part_id, option_dest)
            }
        } else if(part.custom_options !== undefined){
            if(!(part.custom_options in custom_interactions)){
                console.warn(`${part_id}: custom_options broken`)
            }
        } else {
            console.warn(`${part_id}: has no links`);
        }

        if(part.actor !== undefined){
            check_actor(part_id, part.actor);
        }
    }
}
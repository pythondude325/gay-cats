import * as pixi from "pixi.js"

export class GameState {
    character_name: string = "J";
    app: pixi.Application;
    background: pixi.Sprite;

    main_actor: pixi.Sprite;

    lover: string = undefined;

    points: { [index: string]: number };

    constructor(app: pixi.Application){
        this.app = app;

        this.background = new pixi.Sprite();
        this.background.x = 0;
        this.background.y = 0;
        this.background.width = 768;
        this.background.height = 432;
        this.background.zIndex = -100;
        this.app.stage.addChild(this.background);

        this.main_actor = new pixi.Sprite();
        this.main_actor.anchor.y = 1
        this.main_actor.y = 432
        this.main_actor.zIndex = 1;

        this.app.stage.addChild(this.main_actor);

        this.points = {
            "valentino": 0,
            "tigger": 0,
            "debbie": 0,
            "pringle": 0,
            "sammy": 0,
        };
    }
}
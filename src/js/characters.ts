import character_data from "../assets/characters/characters.yml";
import * as assets from './assets';
import * as pixi from 'pixi.js'
import { GameState } from "./gamestate";
import { StateSystem } from "pixi.js";

class CharacterEmotion {
    texture: pixi.Texture;

    constructor(resource_name: string){
        this.texture = pixi.Texture.from(assets.characters[resource_name]);
    }
}

class Character {
    emotions: { [index: string]: CharacterEmotion } = {};

    constructor(source: CharacterData) {
        for(const emotion in source.emotions){
            this.emotions[emotion] = new CharacterEmotion(source.emotions[emotion]);
        }
    }
}

export default class Characters {
    static characters: { [index: string]: Character } = {};

    static load(){
        for(const character_id in character_data){
            Characters.characters[character_id] = new Character(character_data[character_id]);
        }
    }

    static set_main_actor(char: PartActor, state: GameState){
        const character_emotion = Characters.characters[char.id].emotions[char.emotion];
        state.main_actor.texture = character_emotion.texture;
        state.main_actor.height = 350;
        state.main_actor.scale.x = state.main_actor.scale.y;
        state.main_actor.visible = true;

        if(char.flip !== undefined && char.flip){
            state.main_actor.x = 0
            state.main_actor.anchor.x = 0
        } else {
            state.main_actor.x = 768
            state.main_actor.anchor.x = 1
        }
    }

    static hide_main_actor(state: GameState){
        state.main_actor.visible = false;
    }
}


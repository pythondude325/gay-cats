import * as PIXI from 'pixi.js';
import * as assets from './assets';
import * as dialogue from "./dialogue";
import { GameState } from './gamestate';
import Levels from "./levels";
import Characters from './characters';

const app = new PIXI.Application({
    width: 768,
    height: 432,
});

document.getElementById("viewport").appendChild(app.view);

const game_state = new GameState(app);

app.loader.load((loader, resources) => {
    // const dino = new PIXI.Sprite(resources.dino.texture);
    // dino.x = app.renderer.width / 2;
    // dino.y = app.renderer.height / 2;

    // dino.anchor.x = 0.5;
    // dino.anchor.y = 0.5;

    // dino.scale.x = 0.1;
    // dino.scale.y = 0.1;
    
    // app.stage.addChild(dino);

    // app.ticker.add(() => {
    //     dino.rotation += 0.1;
    // });

    Levels.load_levels();
    Characters.load();

    dialogue.goto_part("welcome screen", game_state);
})

function shortcut(node: string){
    dialogue.goto_part(node, game_state)
}

Object.assign(window, {game_state, shortcut, check_dialogue: dialogue.check_dialogue})